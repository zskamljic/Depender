# Depender
![Download](https://api.bintray.com/packages/zskamljic/maven/Depender/images/download.svg?version=1.2.0)
![sonarcloud](https://sonarcloud.io/api/project_badges/measure?project=depender%3Adepend-compiler&metric=coverage)

A simpler dependency injection tool

## Getting started

Add the following to your build.gradle:

```
final depender_version = '1.2.0'

implementation "com.lablizards.depend:depend:$depender_version"
compileOnly "com.lablizards.depend:depend-annotations:$depender_version"
annotationProcessor "com.lablizards.depend:depend-compiler:$depender_version"
```

### Dependencies

Annotate the fields that you want to have injected with `@Depend` annotation like this:

```java
@Depend protected String str;
```

Note that the fields need to be either `protected` or package private.

### Providers

For each field annotated with `@Depend` you need to declare a provider, otherwise the compiler will report an error. Providers are declared as following:

```java
@Provider
public class TestProvider {
    @Provide
    public String provideString() {
        return "test";
    }

    @Provide
    public int provideInt() {
        return 42;
    }
}
```

Only functions annotated with `@Provide` will be taken into account. Every time a dependency is about to be injected the related function with `@Provide` is called, thus if you want to return the same instance every time, simply declare a field in `@Provider` class and return that instead.

### Initialization

After building the project all required classes are generated. You can then use a friendly builder-pattern to initialize the injection class:

```java
DependencyProvider provider = new ConcreteProvider.Builder()
							.setTestProvider(new TestProvider())
							.build();
```

If you missed any required providers in the builder an exception will be thrown, letting you know which one is missing. If you declared a provider, but never use it the method taking it as a parameter is going to be annotated with `@Deprecated`. 

As of 1.2.0 you can also add a chain call to `enableAutoInit()`, which will instantiate all required `@Provider`s that have an empty constructor. The call above can look like this:

```java
DependencyProvider provider = new ConcreteProvider.Builder()
                            .enableAutoInit()
                            .build();
```

### Injecting

The provider we've build in previous step can then be used to provide dependencies to our classes like so:

```java
provider.provide(target)
```

Target in this case can also be `this`, if injecting current class (you can also call it in a common constructor!), or with the instance of an object we wish to inject.

### A simpler way of injection

While other dependency injection tools usually rely on you keeping and acquiring an instance of provider, Depender has a sleek way of doing that:

```java
public class App extends Application implements DependencyProviderContainer {
    private DependencyProvider provider;

    @Override
    public void onCreate() {
        super.onCreate();

        provider = new ConcreteProvider.Builder()
                .enableAutoInit()
                .build();
    }

    @Override
    public DependencyProvider getDependencyProvider() {
        return provider;
    }
}
```

If your application implements the `DependencyProviderContainer` the provider is going to be acquired automatically and you can use `Dependencies.provide(this)` to inject all dependencies without having to resort to static variables in the `Application` class.

## Changelog

- **1.2.0** Add enableAutoInit() to auto-instantiate providers with empty constructors that weren't provided
- **1.1.0** Add methods for unused providers, mark unused as deprecated
- **1.0.0** Initial release

## Authors

* **Zan Skamljic** - Initial work - [zskamljic](https://gitlab.com/zskamljic)


## Licence

```
Copyright 2018 zskamljic

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```