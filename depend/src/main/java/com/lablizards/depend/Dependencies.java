package com.lablizards.depend;

/**
 * Created by zan on 20.5.2017.
 */

public class Dependencies {
    static DependencyProviderContainer container;

    public static void provide(Object dependee) {
        if (container != null && container.getDependencyProvider() != null) {
            container.getDependencyProvider().provide(dependee);
        }
    }
}
