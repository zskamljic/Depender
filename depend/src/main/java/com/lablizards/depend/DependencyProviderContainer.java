package com.lablizards.depend;

/**
 * Created by zan on 20.5.2017.
 */

public interface DependencyProviderContainer {
    DependencyProvider getDependencyProvider();
}
