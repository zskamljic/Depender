package com.lablizards.depend;

public interface DependencyProvider {
    void provide(Object dependee);
}