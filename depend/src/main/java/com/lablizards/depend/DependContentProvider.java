package com.lablizards.depend;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

/**
 * Created by zan on 20.5.2017.
 */

public class DependContentProvider extends ContentProvider {
    @Override
    public boolean onCreate() {
        Context context = getContext().getApplicationContext();
        if (context instanceof DependencyProviderContainer) {
            Dependencies.container = (DependencyProviderContainer) context;
        }
        return false;
    }

    @Override
    public Cursor query(Uri uri,
                        String[] projection,
                        String selection,
                        String[] selectionArgs,
                        String sortOrder) {
        return null;
    }


    @Override
    public String getType(Uri uri) {
        return null;
    }


    @Override
    public Uri insert(Uri uri,
                      ContentValues values) {
        return null;
    }

    @Override
    public int delete(Uri uri,
                      String selection,
                      String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(Uri uri,
                      ContentValues values,
                      String selection,
                      String[] selectionArgs) {
        return 0;
    }
}
