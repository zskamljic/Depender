package com.lablizards.dependdemo;

import android.app.Application;

import com.lablizards.depend.ConcreteProvider;
import com.lablizards.depend.DependencyProvider;
import com.lablizards.depend.DependencyProviderContainer;
import com.lablizards.dependdemo.providers.UnusedProvider;

/**
 * Created by zan on 20.5.2017.
 */

public class App extends Application implements DependencyProviderContainer {
    private DependencyProvider provider;

    @Override
    public void onCreate() {
        super.onCreate();

        provider = new ConcreteProvider.Builder()
                //.setTestProvider(new TestProvider())
                .setUnusedProvider(new UnusedProvider())
                .enableAutoInit()
                .build();
    }

    @Override
    public DependencyProvider getDependencyProvider() {
        return provider;
    }
}
