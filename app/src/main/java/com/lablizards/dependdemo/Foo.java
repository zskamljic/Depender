package com.lablizards.dependdemo;

import com.lablizards.depend.Depend;

/**
 * Created by zan on 15.5.2017.
 */

public class Foo {
    @Depend protected int x;
    @Depend protected String str;
    //    @Depend protected float y;
}
