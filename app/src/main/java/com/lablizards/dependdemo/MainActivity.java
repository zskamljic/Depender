package com.lablizards.dependdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.lablizards.depend.Depend;
import com.lablizards.depend.Dependencies;
import com.lablizards.dependdemo.providers.CProvider;

public class MainActivity extends AppCompatActivity {
    @Depend protected String str;
    @Depend protected CProvider.C c;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Dependencies.provide(this);

        ((TextView) findViewById(R.id.tv_disp)).setText(str);
    }
}
