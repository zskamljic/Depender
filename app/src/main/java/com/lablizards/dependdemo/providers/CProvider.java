package com.lablizards.dependdemo.providers;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;

@Provider
public class CProvider {
    public CProvider(AProvider.A a, BProvider.B b) {

    }

    @Provide
    public C provideC() {
        return new C();
    }

    public class C {}
}
