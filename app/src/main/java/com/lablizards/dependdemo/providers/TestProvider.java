package com.lablizards.dependdemo.providers;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;

/**
 * Created by zan on 15.5.2017.
 */

@Provider
public class TestProvider {
    @Provide
    public String provideString() {
        return "test";
    }

    @Provide
    public int provideInt() {
        return 42;
    }

    public String provideString2() {
        return "test2";
    }
}
