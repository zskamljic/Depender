package com.lablizards.dependdemo.providers;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;

@Provider
public class BProvider {
    @Provide
    public B provideB() {
        return new B();
    }

    class B {}
}
