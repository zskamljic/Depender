package com.lablizards.dependdemo.providers;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;

/**
 * Created by zan on 18.5.2017.
 */

@Provider
public class UnusedProvider {
    @Provide
    public float provideFloat() {
        return 3;
    }
}
