package com.lablizards.dependdemo.providers;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;

@Provider
public class AProvider {
    @Provide
    public A provideA() {
        return new A();
    }

    class A {}
}
