package com.lablizards.depend.codegen.util;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;

/**
 * Created by zan on 17.5.2017.
 */

public class ClassFuncPair {
    public final Element object;
    public final ExecutableElement function;

    public ClassFuncPair(Element object, ExecutableElement function) {
        this.object = object;
        this.function = function;
    }
}
