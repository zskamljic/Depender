package com.lablizards.depend.codegen.util;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

/**
 * Created by zan on 9.1.2019.
 */

public class ClassTypePair {
    public final Element object;
    public final TypeMirror type;

    public ClassTypePair(Element object, TypeMirror function) {
        this.object = object;
        this.type = function;
    }
}
