package com.lablizards.depend.codegen.processors;

import com.lablizards.depend.Depend;
import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.generators.DependingClass;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;

/**
 * Created by zan on 17.5.2017.
 */

public class DependProcessor {
    public Collection<DependingClass> processDependees(RoundEnvironment env) throws ProcessingException {
        Set<? extends Element> dependeeElements = env.getElementsAnnotatedWith(Depend.class);
        HashMap<Element, DependingClass> dependents = new HashMap<>();

        for (Element element : dependeeElements) {
            validateDependee(element);
            addDependency(element, dependents);
        }

        return dependents.values();
    }

    void validateDependee(Element element) throws ProcessingException {
        Element enclosing = element.getEnclosingElement();
        if (enclosing == null || enclosing.getKind() != ElementKind.CLASS) {
            throw new ProcessingException(element, "Only classes can have dependencies.");
        }

        if (element.getModifiers().contains(Modifier.PRIVATE)) {
            throw new ProcessingException(element, "protected or higher access is required.");
        }

        if (element.getModifiers().contains(Modifier.FINAL)) {
            throw new ProcessingException(element, "final fields cannot be dependent.");
        }
    }

    private void addDependency(Element element, HashMap<Element, DependingClass> dependents) {
        Element enclosing = element.getEnclosingElement();
        DependingClass depender =
            dependents.computeIfAbsent(enclosing, k -> new DependingClass(enclosing));

        depender.addDependency(element);
    }
}
