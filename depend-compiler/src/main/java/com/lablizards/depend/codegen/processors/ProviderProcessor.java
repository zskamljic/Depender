package com.lablizards.depend.codegen.processors;

import com.lablizards.depend.Provide;
import com.lablizards.depend.Provider;
import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.util.ClassFuncPair;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

/**
 * Created by zan on 17.5.2017.
 */

public class ProviderProcessor {
    private Elements elementUtils;

    public ProviderProcessor(Elements elementUtils) {
        this.elementUtils = elementUtils;
    }

    public Map<TypeMirror, ClassFuncPair> processProviders(RoundEnvironment env) throws ProcessingException {
        Set<? extends Element> providerElements = env.getElementsAnnotatedWith(Provider.class);

        Map<TypeMirror, ClassFuncPair> providedTypes = new HashMap<>();
        for (Element element : providerElements) {
            validateProvider(element);

            TypeElement providerCandidate = (TypeElement) element;
            List<? extends Element> members = getProviderMethods(providerCandidate);
            processMembers(element, providedTypes, members);
        }

        return providedTypes;
    }

    private void validateProvider(Element element) throws ProcessingException {
        if (!element.getModifiers().contains(Modifier.PUBLIC)) {
            throw new ProcessingException(element,
                "Only public classes can provide dependencies");
        }
    }

    private List<? extends Element> getProviderMethods(TypeElement providerCandidate) {
        return elementUtils.getAllMembers(providerCandidate).stream()
            .filter(e -> e.getAnnotation(Provide.class) != null)
            .collect(Collectors.toList());
    }

    private void processMembers(Element element, Map<TypeMirror, ClassFuncPair> providedTypes,
                                List<? extends Element> members) throws ProcessingException {
        for (Element providerFuncElement : members) {
            ExecutableElement providerFunc = (ExecutableElement) providerFuncElement;

            ensureNoParameters(providerFunc);
            TypeMirror returnType = providerFunc.getReturnType();
            ensureUniqueReturnType(providedTypes, providerFunc, returnType);

            providedTypes.put(returnType, new ClassFuncPair(element, providerFunc));
        }
    }

    private void ensureNoParameters(ExecutableElement providerFunc) throws ProcessingException {
        if (providerFunc.getParameters().isEmpty()) return;
        throw new ProcessingException(providerFunc,
            "Providing functions should not have parameters.");
    }

    private void ensureUniqueReturnType(Map<TypeMirror, ClassFuncPair> providedTypes,
                                        ExecutableElement providerFunc, TypeMirror returnType)
        throws ProcessingException {
        if (!providedTypes.containsKey(returnType)) return;

        throw new ProcessingException(providerFunc,
            "The type %s is already provided in %s",
            returnType.toString(), providedTypes.get(returnType).toString());
    }
}
