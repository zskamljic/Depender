package com.lablizards.depend.codegen.generators;

import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.util.ClassFuncPair;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.TypeElement;
import javax.lang.model.element.VariableElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

/**
 * Created by zskamljic on 18.5.2017.
 */

public class ProviderHolderClass {
    private static final String INIT = "<init>";
    private static final String PACKAGE_NAME = "com.lablizards.depend";
    private static final String DEP_PROVIDER = "ConcreteProvider";
    private static final String BUILDER_NAME = "Builder";
    private static final String BUILDER_VARIABLE = "builder";
    private static final String AUTO_INIT_METHOD = "enableAutoInit";
    private static final String AUTO_INIT_NAME = "autoInit";

    private final Set<DependingClass> fields;
    private final Map<TypeMirror, ClassFuncPair> provided;
    final Set<Element> providers = new HashSet<>();
    final Set<Element> unused = new HashSet<>();
    private Elements elements;

    public ProviderHolderClass(Map<TypeMirror, ClassFuncPair> provided,
                               Collection<DependingClass> dependees) {
        fields = new HashSet<>(dependees);
        this.provided = provided;

        Set<TypeMirror> requiredTypes = getRequiredTypes();
        processProviders(provided, requiredTypes);
    }

    public void generateCode(Elements elementUtils, Filer filer) throws ProcessingException {
        this.elements = elementUtils;
        TypeSpec.Builder classBuilder = createDependencyProvider();

        MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
                .addModifiers(Modifier.PRIVATE);
        constructorBuilder.addParameter(ClassName.get(PACKAGE_NAME, DEP_PROVIDER, BUILDER_NAME), BUILDER_VARIABLE);

        MethodSpec.Builder provideMethod = createProvideMethod();

        for (DependingClass field : fields) {
            TypeName type = field.getClassName(elementUtils);
            String fieldName = field.getGeneratedName().toLowerCase();

            classBuilder.addField(type, fieldName, Modifier.PRIVATE, Modifier.FINAL);

            String parameterString = field.getConstructorParameters().stream()
                    .map(this::toParameterName)
                    .map(s -> BUILDER_VARIABLE + "." + s)
                    .collect(Collectors.joining(", "));

            constructorBuilder.addStatement("this.$L = new $T($L)",
                    fieldName, type, parameterString);

            addProvideStatement(provideMethod, fieldName, field);
        }

        classBuilder.addMethod(constructorBuilder.build());
        classBuilder.addType(generateBuilder());
        classBuilder.addMethod(provideMethod.build());

        try {
            JavaFile.builder(PACKAGE_NAME, classBuilder.build()).build().writeTo(filer);
        } catch (IOException e) {
            throw new ProcessingException(null, e.getMessage());
        }
    }

    private String toParameterName(Element element) {
        String typeName = element.asType().toString();
        typeName = typeName.substring(typeName.lastIndexOf('.') + 1);
        typeName = typeName.substring(0, 1).toLowerCase() + typeName.substring(1, typeName.length());
        return typeName;
    }

    private TypeSpec generateBuilder() throws ProcessingException {
        TypeSpec.Builder classBuilder = TypeSpec.classBuilder(BUILDER_NAME)
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL, Modifier.STATIC);

        classBuilder.addField(boolean.class, AUTO_INIT_NAME, Modifier.PRIVATE);

        Map<Element, String> providerNames = new LinkedHashMap<>();
        generateBuilderSetters(classBuilder, providerNames);

        MethodSpec buildMethod = generateBuildMethod(providerNames);
        classBuilder.addMethod(buildMethod);

        MethodSpec autoInitBuilder = generateAutoInit();
        classBuilder.addMethod(autoInitBuilder);

        return classBuilder.build();
    }

    private MethodSpec generateBuildMethod(Map<Element, String> providerNames) throws ProcessingException {
        MethodSpec.Builder buildMethod = MethodSpec.methodBuilder("build")
                .returns(ClassName.get(PACKAGE_NAME, DEP_PROVIDER))
                .addModifiers(Modifier.PUBLIC);

        generateAutoInitialization(buildMethod, providerNames);
        generateExceptions(buildMethod, providerNames);
        buildMethod.addStatement("return new $L(this)", DEP_PROVIDER);

        return buildMethod.build();
    }

    private MethodSpec generateAutoInit() {
        MethodSpec.Builder autoInit = MethodSpec.methodBuilder(AUTO_INIT_METHOD)
                .returns(ClassName.get(PACKAGE_NAME, DEP_PROVIDER, BUILDER_NAME))
                .addModifiers(Modifier.PUBLIC);

        autoInit.addStatement("this.$L = true", AUTO_INIT_NAME);
        autoInit.addStatement("return this");

        return autoInit.build();
    }

    private void generateAutoInitialization(MethodSpec.Builder buildMethod,
                                            Map<Element, String> providerNames) throws ProcessingException {
        generateDefaultAutoInit(providerNames, buildMethod);
        generateParameterizedAutoInit(providerNames, buildMethod);
    }

    private void generateDefaultAutoInit(Map<Element, String> providerNames, MethodSpec.Builder buildMethod) {
        Set<Element> constructable = providerNames.keySet().stream()
                .filter(this::hasDefaultConstructor)
                .collect(Collectors.toSet());

        if (constructable.isEmpty()) return;

        buildMethod.beginControlFlow("if($L)", AUTO_INIT_NAME);

        for (Element element : constructable) {
            String provider = providerNames.get(element);
            buildMethod.beginControlFlow("if ($L == null)", provider)
                    .addStatement("$L = new $T()", provider, element)
                    .endControlFlow();
        }
        buildMethod.endControlFlow();
    }

    private void generateParameterizedAutoInit(Map<Element, String> providerNames, MethodSpec.Builder buildMethod) throws ProcessingException {
        Set<Element> constructable = providerNames.keySet().stream()
                .filter(e -> findValidConstructor(e) != null)
                .collect(Collectors.toSet());

        if (constructable.isEmpty()) return;

        buildMethod.beginControlFlow("if($L)", AUTO_INIT_NAME);

        for (Element element : constructable) {
            String provider = providerNames.get(element);
            buildMethod.beginControlFlow("if($L == null)", provider)
                    .addStatement("$L = new $T($L)", provider, element, createParameterList(element, providerNames))
                    .endControlFlow();
        }

        buildMethod.endControlFlow();
    }

    private void generateExceptions(MethodSpec.Builder buildMethod, Map<Element, String> providerNames) {
        for (Map.Entry<Element, String> entry : providerNames.entrySet()) {
            if (unused.contains(entry.getKey())) continue;
            addProviderException(buildMethod, entry);
        }
    }

    private void generateBuilderSetters(TypeSpec.Builder classBuilder, Map<Element, String> providerNames) {
        for (Element element : providers) {
            TypeName type = ClassName.get(element.asType());
            String fieldName = element.getSimpleName().toString();
            fieldName = fieldName.substring(0, 1).toLowerCase() + fieldName.substring(1);

            MethodSpec.Builder providerSetter = createSetter(element, type, fieldName);

            if (unused.contains(element)) {
                providerSetter.addAnnotation(Deprecated.class);
            }

            providerNames.put(element, fieldName);
            classBuilder.addField(type, fieldName, Modifier.PRIVATE);

            classBuilder.addMethod(providerSetter.build());
        }
    }

    private Set<TypeMirror> getRequiredTypes() {
        return fields.stream()
                .map(DependingClass::getDependencies)
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    private void processProviders(Map<TypeMirror, ClassFuncPair> provided, Set<TypeMirror> requiredTypes) {
        for (Map.Entry<TypeMirror, ClassFuncPair> entry : provided.entrySet()) {
            if (!requiredTypes.contains(entry.getKey())) {
                unused.add(entry.getValue().object);
            }
            providers.add(entry.getValue().object);
        }
    }

    private TypeSpec.Builder createDependencyProvider() {
        return TypeSpec.classBuilder(DEP_PROVIDER)
                .addSuperinterface(ClassName.get(PACKAGE_NAME,
                        "DependencyProvider"))
                .addModifiers(Modifier.PUBLIC, Modifier.FINAL);
    }

    private MethodSpec.Builder createProvideMethod() {
        return MethodSpec.methodBuilder("provide")
                .addAnnotation(Override.class)
                .addModifiers(Modifier.PUBLIC)
                .addParameter(Object.class, "dependee");
    }

    private void addProvideStatement(MethodSpec.Builder provideMethod, String fieldName, DependingClass field) {
        Element target = field.getEnclosing();
        provideMethod.beginControlFlow("if (dependee instanceof $T)", target)
                .addStatement("$L.provide(($T)dependee)", fieldName, target)
                .endControlFlow();
    }

    private MethodSpec.Builder createSetter(Element element, TypeName type, String fieldName) {
        return MethodSpec.methodBuilder("set" + element.getSimpleName().toString())
                .returns(ClassName.get(PACKAGE_NAME, DEP_PROVIDER, BUILDER_NAME))
                .addModifiers(Modifier.PUBLIC)
                .addParameter(type, fieldName)
                .addStatement("this.$L = $L", fieldName, fieldName)
                .addStatement("return this");
    }

    private void addProviderException(MethodSpec.Builder buildMethod, Map.Entry<Element, String> entry) {
        String provider = entry.getValue();
        String className = provider.substring(0, 1).toUpperCase() + provider.substring(1);

        buildMethod.beginControlFlow("if($L == null)", provider)
                .addStatement("throw new IllegalArgumentException($S)",
                        className + " was not set")
                .endControlFlow();
    }

    private boolean hasDefaultConstructor(Element element) {
        List<? extends Element> members = elements.getAllMembers((TypeElement) element);
        for (Element member : members) {
            if (!(member instanceof ExecutableElement)) continue;

            ExecutableElement function = (ExecutableElement) member;
            if (INIT.equals(function.getSimpleName().toString()) && function.getParameters().isEmpty()) {
                return true;
            }
        }
        return false;
    }

    private ExecutableElement findValidConstructor(Element element) {
        List<? extends Element> members = elements.getAllMembers((TypeElement) element);

        for (Element member : members) {
            if (!(member instanceof ExecutableElement)) continue;

            ExecutableElement function = (ExecutableElement) member;
            if (!INIT.equals(function.getSimpleName().toString()) || function.getParameters().isEmpty()) {
                continue;
            }

            boolean canConstruct = function.getParameters().stream()
                    .map(Element::asType)
                    .allMatch(provided::containsKey);

            if (canConstruct) return function;
        }
        return null;
    }

    private String createParameterList(Element element, Map<Element, String> providerNames) throws ProcessingException {
        ExecutableElement constructor = findValidConstructor(element);
        if (constructor == null) throw new ProcessingException(element, "Constructors changed!");

        List<? extends VariableElement> parameters = constructor.getParameters();

        return parameters.stream()
                .map(p -> {
                    ClassFuncPair provider = provided.get(p.asType());
                    String providerName = providerNames.get(provider.object);
                    String functionName = provider.function.getSimpleName().toString();
                    return String.format("%s.%s()", providerName, functionName);
                })
                .collect(Collectors.joining(", "));
    }
}
