package com.lablizards.depend.codegen.generators;

import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.util.ClassFuncPair;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Element;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

/**
 * Created by zan on 15.5.2017.
 */

public class DependingClass {
    static final String SUFFIX = "$provider";

    private final Element enclosing;
    private final Set<Element> fields;
    private final Set<Element> constructorParameters;

    public DependingClass(Element element) {
        enclosing = element;
        fields = new LinkedHashSet<>();
        constructorParameters = new LinkedHashSet<>();
    }

    public void ensureDependenciesPresent(Collection<TypeMirror> provided) throws ProcessingException {
        for (Element field : fields) {
            if (!provided.contains(field.asType())) {
                throw new ProcessingException(field, "Could not satisfy dependency");
            }
        }
    }

    public void addDependency(Element element) {
        fields.add(element);
    }

    Set<TypeMirror> getDependencies() {
        return fields.stream().map(Element::asType).collect(Collectors.toSet());
    }

    String getGeneratedName() {
        return enclosing.getSimpleName().toString() + SUFFIX;
    }

    TypeName getClassName(Elements elementUtils) {
        PackageElement pkg = elementUtils.getPackageOf(enclosing);
        String packageName = pkg.isUnnamed() ? "" : pkg.getQualifiedName().toString();

        return ClassName.get(packageName, getGeneratedName());
    }

    Set<Element> getConstructorParameters() {
        return constructorParameters;
    }

    public void generateCode(Elements elementUtils, Filer filer, Map<TypeMirror,
        ClassFuncPair> provided) throws ProcessingException {
        String simpleName = getGeneratedName();
        PackageElement pkg = elementUtils.getPackageOf(enclosing);
        String packageName = pkg.isUnnamed() ? "" : pkg.getQualifiedName().toString();

        TypeSpec.Builder classBuilder = TypeSpec.classBuilder(simpleName)
            .addOriginatingElement(enclosing)
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL);

        MethodSpec.Builder provideBuilder = MethodSpec.methodBuilder("provide")
            .addModifiers(Modifier.PUBLIC)
            .addParameter(ClassName.get(enclosing.asType()), "dependee");

        MethodSpec.Builder constructorBuilder = MethodSpec.constructorBuilder()
            .addModifiers(Modifier.PUBLIC);

        Set<TypeName> alreadyProvided = new HashSet<>();

        for (Element field : fields) {
            ClassFuncPair provider = provided.get(field.asType());
            TypeName providerObject = ClassName.get(provider.object.asType());
            String providerName = provider.object.getSimpleName().toString().toLowerCase();

            if (!alreadyProvided.contains(providerObject)) {
                alreadyProvided.add(providerObject);
                classBuilder.addField(providerObject, providerName,
                    Modifier.PRIVATE, Modifier.FINAL);

                constructorBuilder.addParameter(providerObject, providerName)
                    .addStatement("this.$L = $L", providerName, providerName);

                constructorParameters.add(provider.object);
            }
            String fieldName = field.getSimpleName().toString();

            provideBuilder.addStatement("dependee.$L = $L.$L",
                fieldName, providerName, provider.function);
        }

        classBuilder.addMethod(constructorBuilder.build())
            .addMethod(provideBuilder.build());

        try {
            JavaFile.builder(packageName, classBuilder.build()).build().writeTo(filer);
        } catch (IOException e) {
            throw new ProcessingException(enclosing, e.getMessage());
        }
    }

    Element getEnclosing() {
        return enclosing;
    }
}
