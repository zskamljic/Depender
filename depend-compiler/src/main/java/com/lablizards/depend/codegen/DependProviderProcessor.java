package com.lablizards.depend.codegen;

import com.lablizards.depend.Depend;
import com.lablizards.depend.Provider;
import com.lablizards.depend.codegen.generators.DependingClass;
import com.lablizards.depend.codegen.generators.ProviderHolderClass;
import com.lablizards.depend.codegen.processors.DependProcessor;
import com.lablizards.depend.codegen.processors.ProviderProcessor;
import com.lablizards.depend.codegen.util.ClassFuncPair;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.Filer;
import javax.annotation.processing.Messager;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;

public class DependProviderProcessor extends AbstractProcessor {
    private Elements elementUtils;
    private Filer filer;
    private Messager messager;
    private DependProcessor dependProcessor;
    private ProviderProcessor providerProcessor;
    private Map<TypeMirror, ClassFuncPair> provided;
    private Collection<DependingClass> dependees;

    @Override
    public synchronized void init(ProcessingEnvironment processingEnvironment) {
        super.init(processingEnvironment);
        elementUtils = processingEnvironment.getElementUtils();
        filer = processingEnvironment.getFiler();
        messager = processingEnvironment.getMessager();
        dependProcessor = new DependProcessor();
        providerProcessor = new ProviderProcessor(elementUtils);
    }

    @Override
    public boolean process(Set<? extends TypeElement> set, RoundEnvironment env) {
        try {
            provided = providerProcessor.processProviders(env);
            dependees = dependProcessor.processDependees(env);

            if (provided.isEmpty() && dependees.isEmpty()) return true;

            ensureDependenciesPresent();
            generateDependeeProviders();

            generateConcreteProvider();
        } catch (ProcessingException e) {
            e.print(messager);
        }
        return true;
    }

    @Override
    public Set<String> getSupportedAnnotationTypes() {
        return Stream.of(Depend.class, Provider.class)
                .map(Class::getCanonicalName)
                .collect(Collectors.toSet());
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.latestSupported();
    }

    private void generateConcreteProvider() throws ProcessingException {
        ProviderHolderClass providerHolder = new ProviderHolderClass(provided, dependees);
        providerHolder.generateCode(elementUtils, filer);
    }

    private void ensureDependenciesPresent() throws ProcessingException {
        for (DependingClass dependee : dependees) {
            dependee.ensureDependenciesPresent(provided.keySet());
        }
    }

    private void generateDependeeProviders() throws ProcessingException {
        for (DependingClass dependee : dependees) {
            dependee.generateCode(elementUtils, filer, provided);
        }
    }
}
