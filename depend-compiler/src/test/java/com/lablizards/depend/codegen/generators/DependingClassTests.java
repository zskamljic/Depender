package com.lablizards.depend.codegen.generators;

import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.util.ClassFuncPair;
import com.squareup.javapoet.TypeName;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.io.Writer;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.annotation.processing.Filer;
import javax.lang.model.element.Element;
import javax.lang.model.element.Name;
import javax.lang.model.element.PackageElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;
import javax.tools.JavaFileObject;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DependingClassTests {
    private Element element;
    private DependingClass dependingClass;
    private PackageElement packageElement;
    private Name name;
    private Elements utils;

    @Before
    public void setUp() {
        element = mock(Element.class);
        dependingClass = new DependingClass(element);

        name = mock(Name.class);
        utils = mock(Elements.class);
        packageElement = mock(PackageElement.class);

        when(name.toString()).thenReturn("test");
        when(element.getSimpleName()).thenReturn(name);
        when(utils.getPackageOf(any())).thenReturn(packageElement);
    }

    @Test
    public void getFirstMissingDependency_noMissingFields() throws ProcessingException {
        dependingClass.ensureDependenciesPresent(new HashSet<>());
    }

    @Test(expected = ProcessingException.class)
    public void getFistMissingDependency_noDependenciesAndRequiredField() throws ProcessingException {
        dependingClass.addDependency(mock(Element.class));

        dependingClass.ensureDependenciesPresent(new HashSet<>());
    }

    @Test(expected = ProcessingException.class)
    public void getFirstMissingDependency_differentProvided() throws ProcessingException {
        Element field = mock(Element.class);

        dependingClass.addDependency(field);

        Set<TypeMirror> provided = new HashSet<>();
        provided.add(mock(TypeMirror.class));
        dependingClass.ensureDependenciesPresent(provided);
    }

    @Test
    public void getDependencies_returnsSet() {
        Element element = mock(Element.class);

        dependingClass.addDependency(element);

        Set<TypeMirror> dependencies = dependingClass.getDependencies();

        assertEquals(1, dependencies.size());
        assertEquals(element.asType(), dependencies.iterator().next());
    }

    @Test
    public void getGeneratedName() {
        String className = dependingClass.getGeneratedName();

        assertEquals("test" + DependingClass.SUFFIX, className);
    }

    @Test
    public void getClassName_noPackage() {
        when(packageElement.isUnnamed()).thenReturn(true);

        TypeName name = dependingClass.getClassName(utils);
        assertEquals("test" + DependingClass.SUFFIX, name.toString());
    }

    @Test
    public void getClassName_withPackage() {
        when(packageElement.isUnnamed()).thenReturn(false);
        when(packageElement.getQualifiedName()).thenReturn(name);

        TypeName name = dependingClass.getClassName(utils);
        assertEquals("test.test" + DependingClass.SUFFIX, name.toString());
    }

    @Test
    public void getConstructorParameters() {
        assertTrue(dependingClass.getConstructorParameters().isEmpty());
    }

    @Test
    public void generateCode() throws ProcessingException, IOException {
        TypeMirror type = mock(TypeMirror.class);
        when(type.accept(any(), any())).thenReturn(mock(TypeName.class));

        when(packageElement.isUnnamed()).thenReturn(true);
        when(element.asType()).thenReturn(type);

        Map<TypeMirror, ClassFuncPair> provided = new HashMap<>();

        Filer filer = mock(Filer.class);
        JavaFileObject javaFileObject = mock(JavaFileObject.class);

        when(filer.createSourceFile(any(), any())).thenReturn(javaFileObject);
        when(javaFileObject.openWriter()).thenReturn(mock(Writer.class));

        dependingClass.generateCode(utils, filer, provided);
    }

    @Test
    public void getEnclosing() {
        Element enclosing = dependingClass.getEnclosing();

        assertEquals(element, enclosing);
    }
}
