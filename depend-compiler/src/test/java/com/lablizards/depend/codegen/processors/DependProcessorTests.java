package com.lablizards.depend.codegen.processors;

import com.lablizards.depend.Depend;
import com.lablizards.depend.codegen.ProcessingException;
import com.lablizards.depend.codegen.generators.DependingClass;

import org.junit.Before;
import org.junit.Test;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.Modifier;
import javax.lang.model.util.Elements;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DependProcessorTests {
    private DependProcessor processor;
    private Element enclosingClass;
    private Element element;

    @Before
    public void setUp() {
        processor = new DependProcessor();
        enclosingClass = mock(Element.class);
        when(enclosingClass.getKind()).thenReturn(ElementKind.CLASS);

        element = mock(Element.class);
        when(element.getEnclosingElement()).thenReturn(enclosingClass);
    }

    @Test
    public void processDependees() throws ProcessingException {
        Set<Element> elements = new HashSet<>();
        elements.add(element);

        RoundEnvironment env = mock(RoundEnvironment.class);
        doReturn(elements).when(env).getElementsAnnotatedWith(Depend.class);

        Collection<DependingClass> result = processor.processDependees(env);
        assertEquals(1, result.size());
    }

    @Test(expected = ProcessingException.class)
    public void validateDependee_throwsWhenNotClass() throws ProcessingException {
        when(enclosingClass.getKind()).thenReturn(ElementKind.INTERFACE);

        processor.validateDependee(element);
    }

    @Test(expected = ProcessingException.class)
    public void validateDependee_throwsWhenPrivate() throws ProcessingException {
        Set<Modifier> modifiers = new HashSet<>();
        modifiers.add(Modifier.PRIVATE);

        when(element.getModifiers()).thenReturn(modifiers);

        processor.validateDependee(element);
    }

    @Test(expected = ProcessingException.class)
    public void validateDependee_throwsWhenFinal() throws ProcessingException {
        Set<Modifier> modifiers = new HashSet<>();
        modifiers.add(Modifier.FINAL);

        when(element.getModifiers()).thenReturn(modifiers);

        processor.validateDependee(element);
    }
}
