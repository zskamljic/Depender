package com.lablizards.depend.codegen;

import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourcesSubjectFactory;

import org.junit.Test;

import java.util.Arrays;

import javax.tools.JavaFileObject;

public class DependProviderProcessorTests {
    private final JavaFileObject INTERFACE = JavaFileObjects.forSourceLines(
            "com.lablizards.depend.DependencyProvider",
            "package com.lablizards.depend;",
            "",
            "public interface DependencyProvider {",
            "    void provide(Object dependee);",
            "}"
    );

    private final JavaFileObject DEPENDEE = JavaFileObjects.forSourceLines(
            "test.Dependee",
            "package test;",
            "",
            "import com.lablizards.depend.Depend;",
            "",
            "public class Dependee {",
            "   @Depend String depending;",
            "}"
    );

    private final JavaFileObject PROVIDER = JavaFileObjects.forSourceLines(
            "test.TestProvider",
            "package test;",
            "",
            "import com.lablizards.depend.Provider;",
            "import com.lablizards.depend.Provide;",
            "",
            "@Provider",
            "public class TestProvider {",
            "   @Provide",
            "   public String getString() {",
            "       return \"test\";",
            "   }",
            "}"
    );
    private final JavaFileObject DEPENDEE_RESULT = JavaFileObjects.forSourceLines(
            "test.Dependee$provider",
            "package test;",
            "",
            "public final class Dependee$provider {",
            "   private final TestProvider testprovider;",
            "",
            "   public Dependee$provider(TestProvider testprovider) {",
            "       this.testprovider = testprovider;",
            "   }",
            "",
            "   public void provide(Dependee dependee) {",
            "       dependee.depending = testprovider.getString();",
            "   }",
            "}"
    );

    private final JavaFileObject CONCRETE_PROVIDER_NO_DEPENDS = JavaFileObjects.forSourceLines(
            "com.lablizards.depend.ConcreteProvider",
            "package com.lablizards.depend;",
            "",
            "import java.lang.Deprecated;",
            "import java.lang.Object;",
            "import java.lang.Override;",
            "import test.TestProvider",
            "",
            "public final class ConcreteProvider implements DependencyProvider {",
            "   private ConcreteProvider(Builder builder) {",
            "   }",
            "",
            "   @Override",
            "   public void provide(Object dependee) {",
            "   }",
            "",
            "   public static final class Builder {",
            "       private boolean autoInit;",
            "       private TestProvider testProvider;",
            "",
            "       @Deprecated",
            "       public Builder setTestProvider(TestProvider testProvider) {",
            "           this.testProvider = testProvider;",
            "           return this;",
            "       }",
            "       public ConcreteProvider build() {",
            "           return new ConcreteProvider(this);",
            "       }",
            "       public Builder enableAutoInit() {",
            "           this.autoInit = true;",
            "           return this;",
            "       }",
            "   }",
            "}"
    );

    private final JavaFileObject CONCRETE_PROVIDER_HAS_DEPENDS = JavaFileObjects.forSourceLines(
            "com.lablizards.depend.ConcreteProvider",
            "package com.lablizards.depend;",
            "",
            "import java.lang.Object;",
            "import java.lang.Override;",
            "import test.Dependee;",
            "import test.Dependee$provider;",
            "import test.TestProvider;",
            "",
            "public final class ConcreteProvider implements DependencyProvider {",
            "   private final Dependee$provider dependee$provider;",
            "",
            "   private ConcreteProvider(Builder builder) {",
            "       this.dependee$provider = new Dependee$provider(builder.testProvider);",
            "   }",
            "",
            "   @Override",
            "   public void provide(Object dependee) {",
            "       if (dependee instanceof Dependee) {",
            "           dependee$provider.provide((Dependee)dependee);",
            "       }",
            "   }",
            "",
            "   public static final class Builder {",
            "       private boolean autoInit;",
            "       private TestProvider testProvider;",
            "",
            "       public Builder setTestProvider(TestProvider testProvider) {",
            "           this.testProvider = testProvider;",
            "           return this;",
            "       }",
            "       public ConcreteProvider build() {",
            "           if(autoInit) {",
            "               if (testProvider == null) {",
            "                   testProvider = new TestProvider();",
            "               }",
            "           }",
            "           if (testProvider == null) {",
            "               throw new IllegalArgumentException(\"TestProvider was not set\");",
            "           }",
            "           return new ConcreteProvider(this);",
            "       }",
            "       public Builder enableAutoInit() {",
            "           this.autoInit = true;",
            "           return this;",
            "       }",
            "   }",
            "}"
    );

    @Test
    public void generateClassProvider() {
        Truth.assert_()
                .about(JavaSourcesSubjectFactory.javaSources())
                .that(Arrays.asList(INTERFACE, DEPENDEE, PROVIDER))
                .processedWith(new DependProviderProcessor())
                .compilesWithoutError()
                .and()
                .generatesSources(DEPENDEE_RESULT);
    }

    @Test
    public void generateConcreteProvider() {
        Truth.assert_()
                .about(JavaSourcesSubjectFactory.javaSources())
                .that(Arrays.asList(INTERFACE, PROVIDER))
                .processedWith(new DependProviderProcessor())
                .compilesWithoutError()
                .and()
                .generatesSources(CONCRETE_PROVIDER_NO_DEPENDS);
    }

    @Test
    public void failsWhenNoProvider() {
        Truth.assert_()
                .about(JavaSourcesSubjectFactory.javaSources())
                .that(Arrays.asList(INTERFACE, DEPENDEE))
                .processedWith(new DependProviderProcessor())
                .failsToCompile();
    }

    @Test
    public void generateConcreteProvider_generatesConstructor() {
        Truth.assert_()
                .about(JavaSourcesSubjectFactory.javaSources())
                .that(Arrays.asList(INTERFACE, PROVIDER, DEPENDEE))
                .processedWith(new DependProviderProcessor())
                .compilesWithoutError()
                .and()
                .generatesSources(CONCRETE_PROVIDER_HAS_DEPENDS);
    }
}
