package com.lablizards.depend.codegen.generators;

import com.lablizards.depend.codegen.util.ClassFuncPair;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.type.TypeMirror;

import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;

public class ProviderHolderClassTest {
    private Map<TypeMirror, ClassFuncPair> provided = new HashMap<>();
    private List<DependingClass> dependees = new ArrayList<>();
    private Element unused;

    @Before
    public void setUp() {
        Element object = mock(Element.class);
        for (int i = 0; i < 3; i++) {
            TypeMirror mirror = mock(TypeMirror.class);
            ClassFuncPair pair = new ClassFuncPair(object, mock(ExecutableElement.class));

            provided.put(mirror, pair);
        }

        unused = object;
        DependingClass dependingClass = mock(DependingClass.class);

        Set<Element> dependencies = new HashSet<>();
        dependencies.add(unused);
        dependees.add(dependingClass);

        doReturn(dependencies).when(dependingClass).getDependencies();
    }

    @Test
    public void setProviders() {
        ProviderHolderClass holder = new ProviderHolderClass(provided, dependees);

        Element[] providerObjects = provided.values().stream()
                .map(p -> p.object)
                .distinct()
                .toArray(Element[]::new);
        assertArrayEquals(providerObjects, holder.providers.toArray());
    }

    @Test
    public void setUnused() {
        ProviderHolderClass holder = new ProviderHolderClass(provided, dependees);

        assertArrayEquals(new Object[]{unused}, holder.unused.toArray());
    }
}
