package com.lablizards.depend.codegen;

import org.junit.Before;
import org.junit.Test;

import javax.annotation.processing.Messager;
import javax.lang.model.element.Element;
import javax.tools.Diagnostic;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class ProcessingExceptionTests {
    private Element element;
    private Messager messager;

    @Before
    public void setUp() {
        element = mock(Element.class);
        messager = spy(Messager.class);
    }

    @Test
    public void print_NoParams_rawString() {
        String message = "test";

        ProcessingException exception = new ProcessingException(element, message);

        exception.print(messager);

        verify(messager, times(1)).printMessage(Diagnostic.Kind.ERROR,
            message, element);
    }

    @Test
    public void print_Params() {
        String message = "%s";
        String value = "mock";

        ProcessingException exception = new ProcessingException(element, message, value);
        exception.print(messager);

        verify(messager, times(1)).printMessage(Diagnostic.Kind.ERROR,
            String.format(message, value), element);
    }
}
